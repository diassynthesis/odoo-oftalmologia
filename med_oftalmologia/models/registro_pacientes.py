# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#    GRP Estado Uruguay
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api


class RegistroPaciente(models.Model):
    _name = 'registro.paciente'
    _rec_name = 'name'
    _description = 'Registro de datos del paciente'

    name = fields.Char('Nombre Completo',
                       compute='_compute_name',
                       store=True)
    primer_nombre = fields.Char('Primer Nombre', required=True)
    segundo_nombre = fields.Char('Segundo Nombre')
    primer_apellido = fields.Char('Primer Apellido', required=True)
    segundo_apellido = fields.Char('Segundo Apellido')
    edad = fields.Integer('Edad')
    sexo = fields.Selection(string='sexo', selection=[('f', 'Femenino'),
                                                      ('m', 'Masculino')])
    edo_civil = fields.Selection(string='Estado Civil', selection=[
                                                  ('c', 'Casado'),
                                                  ('s', 'Soltero'),
                                                  ('v', 'Viudo'),
                                                  ('d', 'Divorciado')
                                                  ])

    ocupacion = fields.Char(u'Ocupación')
    fecha_nacimiento = fields.Date('Fecha de Nacimiento')
    calle_numero = fields.Char(u'Calle y Número')
    colonia = fields.Char('Colonia')
    delegacion = fields.Char(u'Delegación')
    cp = fields.Char('C.P')
    telefono1 = fields.Char('Telefono 1')
    telefono2 = fields.Char('Telefono 2')
    mail = fields.Char('Correo Electronico')

    @api.multi
    def name_get(self):
        res = super(RegistroPaciente, self).name_get()
        data = []
        for record in self:
            nombre_paciente = record.primer_nombre + ' ' + \
                              record.primer_apellido
            data.append((record.id, nombre_paciente))
        return data

    @api.depends('primer_nombre', 'primer_apellido')
    def _compute_name(self):
        for record in self:
            if record.primer_nombre and record.primer_apellido:
                record.name = record.primer_nombre + ' ' + \
                              record.primer_apellido

    @api.model
    def create(self, values):
        paciente = super(RegistroPaciente, self).create(values)
        partner = self.env['res.partner']
        for record in paciente:
            partner.create({'name':
                            record.primer_nombre + ' ' +
                            record.primer_apellido})
        return paciente
