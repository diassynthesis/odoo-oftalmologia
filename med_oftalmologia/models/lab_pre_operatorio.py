# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api


class LabPreOperatorios(models.Model):
    _name = 'lab.preoperatorios'
    _description = 'Registro Para creacion de Laboratorios Pre-Operatorios'

    fecha = fields.Date('Fecha')
    expediente_id = fields.Many2one('expediente.clinico', 'Expediente')
    paciente_id = fields.Many2one('registro.paciente', readonly='True',
                                  compute='_compute_id')
    nom_paciente = fields.Char('Nombre del Paciente', readonly='True',
                               compute='_compute_name')
    rel_sexo = fields.Selection(string='Sexo', selection=[('f', 'Femenino'),
                                                          ('m', 'Masculino')],
                                readonly='True', related='paciente_id.sexo')
    rel_edad = fields.Integer(string='Edad', related='paciente_id.edad',
                              readonly="True")
    fecha_estudio = fields.Date('Fecha Estudio')
    biometria_hema = fields.Boolean(u'Biometria Hemática')
    quim_sangui = fields.Boolean(u'Quimica Sanguinea (Glucosa/ Urea/ Creatina)'
                                 )
    ecg = fields.Boolean('ECG (Electrocardiograma)')
    otros = fields.Text('Otros')
    tp = fields.Boolean('TP')
    ttp = fields.Boolean('TTP')

    @api.multi
    @api.depends('expediente_id')
    def _compute_name(self):
        for record in self:
            record.nom_paciente = record.expediente_id.paciente_id.name

    @api.multi
    @api.depends('expediente_id')
    def _compute_id(self):
        for record in self:
            record.paciente_id = record.expediente_id.paciente_id.id
