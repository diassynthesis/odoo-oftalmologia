# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api


class ExploracionOptometrica(models.Model):
    _name = 'exploracion.optometrica'
    _description = u'Exploracion Optométrica'

    venta_lentes_id = fields.Many2one('venta.lentes')
    expediente_id = fields.Many2one('expediente.clinico')
    fecha = fields.Date('Fecha')
    optometrista_id = fields.Many2one('registro.doctores', 'Optometrista')
    sin_rx_od_lejos = fields.Char('Ojo Derecho Lejos')
    sin_rx_od_ph = fields.Char('Ojo Derecho PH')
    sin_rx_od_cerca = fields.Char('Ojo Derecho Cerca')
    sin_rx_oi_lejos = fields.Char('Ojo Izquierdo Lejos')
    sin_rx_oi_ph = fields.Char('Ojo Izquierdo PH')
    sin_rx_oi_cerca = fields.Char('Ojo Izquierdo Cerca')
    sin_rx_ad_lejos = fields.Char('AO Lejos')
    sin_rx_ad_cerca = fields.Char('AO Cerca')
    con_rx_od_lejos = fields.Char('Ojo Derecho Lejos')
    con_rx_od_cerca = fields.Char('Ojo Derecho Cerca')
    con_rx_oi_lejos = fields.Char('Ojo Izquierdo Lejos')
    con_rx_oi_cerca = fields.Char('Ojo Izquierdo Cerca')
    con_rx_ad_lejos = fields.Char('AO Lejos')
    con_rx_ad_cerca = fields.Char('AO Cerca')
    dip = fields.Char('Dip')
    refra_queratometrica_od = fields.Char(u'Queratometrías Ojo Derecho',
                                          default='00:00 x 0° / 00:00 x 0°')
    refra_queratometrica_oi = fields.Char(u'Queratometrías Ojo Izquierdo',
                                          default='00:00 x 0° / 00:00 x 0°')
    refra_ref_objetiva_od = fields.Char('Refraccion Objetiva Ojo Derecho')
    refra_ref_objetiva_oi = fields.Char('Refraccion Objetiva Ojo Izquierdo')
    refra_ref_final_ids = fields.One2many('refraccion.final', 'exploracion_id',
                                          'Refraccion Final')
    refra_av_rx_lejana_od = fields.Char('AV con RX Lejana Ojo Derecho')
    refra_av_rx_lejana_oi = fields.Char('AV con RX Lejana Ojo Izquierdo')
    refra_add_od = fields.Char('ADD Ojo Derecho')
    refra_add_oi = fields.Char('ADD Ojo Izquierdo')
    refra_av_rx_cercana_od = fields.Char('AV con RX Cercana Ojo Derecho')
    refra_av_rx_cercana_oi = fields.Char('AV con RX Cercana Ojo Izquierdo')
    refra_ambulatoria_od = fields.Char('Ambulatoria Ojo Derecho')
    refra_ambulatoria_oi = fields.Char('Ambulatoria Ojo Izquierdo')

    od_esfera = fields.Char('Ojo Derecho Esfera')
    od_cilindro = fields.Char('Ojo Derecho Cilindro')
    od_eje = fields.Char('Ojo Derecho Eje')
    od_add = fields.Char('Ojo Derecho Add')
    od_tipo = fields.Char('Ojo Derecho Tipo')
    od_tx = fields.Char('Ojo Derecho TX')

    oi_esfera = fields.Char('Ojo Izquierdo Esfera')
    oi_cilindro = fields.Char('Ojo Izquierdo Cilindro')
    oi_eje = fields.Char('Ojo Izquierdo Eje')
    oi_add = fields.Char('Ojo Izquierdo Add')
    oi_tipo = fields.Char('Ojo Izquierdo Tipo')
    oi_tx = fields.Char('Ojo Izquierdo TX')
    recom_opto = fields.Text('Recomendaciones Optometrista')
    

class RefraccionFinal(models.Model):
    _name = 'refraccion.final'
    _description = 'Refraccion Final'

    exploracion_id = fields.Many2one('exploracion.optometrica')
    esf = fields.Char('Esf')
    cil = fields.Char('Cil')
    eje = fields.Char('Eje')
    ojo = fields.Selection(string="Ojo",
                           selection=[('Derecho', 'Ojo Derecho'),
                                      ('Izquierdo', 'Ojo Izquierdo')],
                           required="True")
