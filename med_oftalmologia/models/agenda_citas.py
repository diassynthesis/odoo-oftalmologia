# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta


class AgendaCitas(models.Model):
    _name = 'agenda.citas'
    _description = 'Agenda Para citas de Pacientes'

    state = fields.Selection(string="Estado",
                             selection=[('b', 'Borrador'),
                                        ('p', 'Pendiente'),
                                        ('c', 'Confirmada'),
                                        ('ca', 'Cancelada'),
                                        ('re', 'Reprogramada')])
    fecha_cita = fields.Datetime("Fecha y Hora Inicio", required=True, defautl=datetime.today())
    fecha_cita_2 = fields.Datetime("Fecha y Hora Fin", readonly=True,
                                   compute='_compute_fecha', store=True)
    doctor_id = fields.Many2one('registro.doctores', 'Doctor')
    paciente_id = fields.Many2one('registro.paciente', 'Paciente')
    tipo_cita = fields.Char('Tipo de Cita')
    observaciones = fields.Text('Observaciones')
    referencia_id = fields.Many2one('agenda.referencias')

    @api.one
    @api.depends('fecha_cita')
    def _compute_fecha(self):
        for record in self:
            if record.fecha_cita:
                record.fecha_cita_2 = (datetime.strptime(record.fecha_cita,
                                                         '%Y-%m-%d %H:%M:%S')
                                       + relativedelta(minutes=+ 30))


class Referencias(models.Model):
    _name = 'agenda.referencias'
    _rec_name = 'refiere'
    _description = 'Registros de Referentes'

    refiere = fields.Char('Refiere')
    telefono_ids = fields.One2many('referencia.telefono',
                                   'referencia_id')
    direccion_ids = fields.One2many('referencia.direccion',
                                    'referencia_id')
    horario = fields.Char('Horario en que se Localiza')
    tipo_convenio = fields.Char('Tipo de Convenio')
    observaciones = fields.Char('Observaciones')


class ReferenciaDireccion(models.Model):
    _name = 'referencia.direccion'
    _rec_name = 'direccion'
    _description = 'Direccion del referente'

    referencia_id = fields.Many2one('agenda.referencias')
    direccion = fields.Char('Direccion')


class ReferenciaTelefono(models.Model):
    _name = 'referencia.telefono'
    _rec_name = 'telefono'
    _description = 'Telefono del Referente'

    referencia_id = fields.Many2one('agenda.referencias')
    telefono = fields.Char('Telefono')

