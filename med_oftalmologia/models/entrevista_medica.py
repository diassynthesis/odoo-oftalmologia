# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api


class EntrevistaMedica(models.Model):
    _name = 'entrevista.medica'
    _rec_name = 'cod_entrevista'
    _description = 'Entrevista Medica del Paciente'

    paciente_id = fields.Many2one('registro.paciente', 'Paciente')
    cod_entrevista = fields.Char('Codigo de la entrevista')
    tiene_mascota = fields.Char('Tiene Mascota?')
    tipo_mascota = fields.Char('Tipo Mascota')
    comidas_dia = fields.Char('Cuantas Comidas al Dia?')
    tipo_comida = fields.Char('Tipo de Comida acostumbrada?')
    fuma = fields.Char('Fuma?')
    frecuencia_fuma = fields.Char('Con que frecuente fuma?')
    alcohol = fields.Char('Bebe?')
    frecuencia_alcohol = fields.Char('Con que frecuencia bebe?')
    usa_estupefaciente = fields.Char('Usa algun estupefaciente?')
    motivo_consulta = fields.Text('Motivo de consulta')
    salud_general = fields.Text(u'Antecendetes Personales no patológicoss',  
                                default="No refiere antecedentes "
                                        "no patológicos",
                                        help="Ingesta de alcohol, tabaquismo, "
                                             "vacunación, alergias")
    salud_ocultar = fields.Text(u'Antecedentes Oftalmológicos',
                                default="No refiere antecedentes "
                                        "oftalmologicos",
                                        help="Uso de lentes, cirugías "
                                             "oculares, tratamientos")
    historia_optica = fields.Text(u'Historia Óptica')
    antecedentes_familiares = fields.Text('Antecedentes Heredo Familiares',
                                          default="No refiere antecedentes "
                                                  "heredero familaires")
    salud_familiar_ocular = fields.Text('Salud Familiar Ocular y Refractiva')
    habitus_exterior = fields.Char('Habitus Exterior')
    presencia_normal = fields.Char('Presencia Normal')
    med_actual = fields.Char(u'Medicación Actual', default="Sin Tratamiento")
    antecedente_nopato = fields.Char('Antecedentes Personales Patologicos',
                                     default="No refiere antecedentes "
                                             "personales patológicos",
                                     help="Enfermedades graves, cáncer, "
                                          "diabetes, hipertensión, "
                                          "hospitalización o cirugías")
