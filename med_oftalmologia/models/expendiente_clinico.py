# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api


class ExpendienteClinico(models.Model):
    _name = 'expediente.clinico'
    _rec_name = 'numero_expediente'
    _description = 'Contiene Evalaciones del Paciente'
    _order = 'rel_nombre_paciente desc'

    numero_expediente = fields.Char()
    paciente_id = fields.Many2one('registro.paciente', 'Paciente')
    rel_nombre_paciente = fields.Char('Nombre del Paciente',
                                      related='paciente_id.name',
                                      readonly=True, store=True)
    rel_sexo = fields.Selection(string='Sexo', selection=[('f', 'Femenino'),
                                                          ('m', 'Masculino')],
                                related='paciente_id.sexo', readonly='True')
    rel_edad = fields.Integer(string='Edad', related='paciente_id.edad',
                              readonly='True')

    interrogatorio_ids = fields.Many2one('entrevista.medica', 'Interrogatorio')
    oftalmologia_ids = fields.One2many('oftalmologia', 'expediente_id',
                                       'Exploracion Optometrica')
    exploracion_optometrica_ids = fields.One2many('exploracion.optometrica',
                                                  'expediente_id',
                                                  'Exploracion Optometrica')
    venta_lentes_ids = fields.One2many('venta.lentes',
                                       'expediente_id',
                                       'Lentes')
    nota_evolutiva_ids = fields.One2many('nota.evolutiva',
                                         'expediente_id',
                                         'Nota Evolutiva')
    nota_estudio_ids = fields.One2many('nota.estudio',
                                       'expediente_id',
                                       'Nota Estudio')
    estudios_oftalmologicos_ids = fields.One2many("estudios.oftalmologicos",
                                                  "expediente_id",
                                                  "Estudios Ofalmologicos")
    estudios_pre_operatorios_ids = fields.One2many('lab.preoperatorios',
                                                   'expediente_id',
                                                   'Estuidos Preoperatorios')
    nota_privacidad_ids = fields.One2many('nota.privacidad', 'expediente_id')

    @api.model
    def create(self, values):
        expediente = self.env['entrevista.medica']
        etiqueta = 'EXP' + '-'
        expediente.create({
            'cod_entrevista': etiqueta,
            'paciente_id': values['paciente_id']
        })
        return super(ExpendienteClinico, self).create(values)


