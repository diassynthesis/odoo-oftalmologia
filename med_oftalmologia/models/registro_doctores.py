# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api


class RegistroDoctores(models.Model):
    _name = 'registro.doctores'
    _rec_name = 'name'
    _description = 'New Description'

    name = fields.Char('Nombre Del Doctor', compute='_compute_name')
    primer_nombre = fields.Char('Primer Nombre', required=True)
    segundo_nombre = fields.Char('Segundo Nombre')
    primer_apellido = fields.Char('Primer Apellido', required=True)
    segundo_apellido = fields.Char('Segundo Apellido')
    cedula = fields.Char('Cedula Profesional Doctor')
    edad = fields.Integer('Edad')
    sexo = fields.Selection(string='Sexo', selection=[('f', 'Femenino'),
                                                      ('m', 'Masculino')])

    @api.multi
    def _compute_name(self):
        for record in self:
            if record.primer_nombre:
                if record.sexo == 'f':
                    titulo = 'Dra.'
                else:
                    titulo = 'Dr.'
                record.name = titulo + ' ' + record.primer_nombre + ' ' + \
                              record.primer_apellido
