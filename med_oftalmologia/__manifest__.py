# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Enterprise Management Solution
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name':     u'med_oftalmologia',
    'author':   'Luis Contreras',
    'category': 'GRP',
    'license':  'AGPL-3',
    'version':  '1.0',
    'description': u"""Modulo para almacenar la historia clinica de los 
                    pacientes""",
    'depends': ['sale', 'sale_management', 'report_xlsx'],
    'data': ['reports/report_nota_optometrica.xml',
             'reports/report_venta_lentes.xml',
             'reports/report_nota_laboratorio.xml',
             'reports/report_resumen_clinico.xml',
             'reports/report_receta_medica.xml',
             'reports/report_receta_medica_evolutiva.xml',
             'reports/report_estudios_oftalmologicos.xml',
             'reports/estudios_preoperatorios.xml',
             'views/registro_pacientes.xml', 'views/expediente_clinico.xml',
             'views/registro_doctores.xml',  'views/refraccion_final.xml',
             'views/agenda_citas.xml',
             'views/examenes_preoperatorios_view.xml',
             'views/interrogatorio.xml',
             'views/exploracion_optometrica.xml',
             'views/nota_oftalmologica.xml', 'views/venta_lentas.xml',
             'views/nota_evolutiva.xml', 'views/nota_estudio.xml',
             'views/estudios_oftalmologico.xml',
             'views/medicamento_nota_evolutiva.xml',
             'views/medicamento_oftalmologia.xml',
             'views/nota_privacidad.xml',
             'security/clinica_groups.xml',
             'security/ir.model.access.csv',
             'reports/report_paciente_xlsx.xml',
             ]
    ,
    'auto_install': False,
    'installable': True,
    'application': False
}
